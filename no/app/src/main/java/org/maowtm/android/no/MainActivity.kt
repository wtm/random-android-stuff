package org.maowtm.android.no

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import org.maowtm.android.no.Utils.Companion.date_fmt
import org.maowtm.android.no.Utils.Companion.parse_date
import org.maowtm.android.no.Utils.Companion.today
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.IllegalArgumentException
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.collections.ArrayList

class VM() : ViewModel() {
    fun get_last(ctx: Context): Calendar? {
        return DB.db(ctx).use {
            it.rawQuery("""SELECT "date" FROM "no" ORDER BY "date" DESC LIMIT 1;""", arrayOf()).use { res ->
                if (res.count == 0) {
                    null
                } else {
                    res.moveToFirst()
                    parse_date(res.getString(0))
                }
            }
        }
    }

    fun date_has_no(ctx: Context, date: Calendar): Boolean {
        return DB.db(ctx).use {
            it.rawQuery("""SELECT null FROM "no" WHERE "date" = ?;""", arrayOf(date_fmt.format(date.time))).use { it.count > 0 }
        }
    }

    fun get_all_since(ctx: Context, since: Calendar): List<Calendar> {
        val res = ArrayList<Calendar>()
        DB.db(ctx).use {
            it.rawQuery("""SELECT "date" FROM "no" WHERE "date" >= ? ORDER BY "date" ASC;""", arrayOf(date_fmt.format(since.time))).use { c ->
                c.moveToFirst()
                while (!c.isAfterLast) {
                    res.add(parse_date(c.getString(0)))
                    c.moveToNext()
                }
            }
        }
        return res;
    }

    fun date_put_no(ctx: Context, date: Calendar) {
        if (date_has_no(ctx, date)) return;
        DB.db(ctx).use {
            it.execSQL("""INSERT INTO "no" VALUES (?);""", arrayOf(date_fmt.format(date.time)))
        }
    }

    private var cached_init_date: Calendar? = null
    fun get_init_date(ctx: Context): Calendar {
        if (cached_init_date != null) {
            return cached_init_date!!
        } else {
            cached_init_date = DB.db(ctx).use {
                it.rawQuery("""SELECT "date" FROM "init";""", arrayOf()).use {
                    it.moveToFirst()
                    parse_date(it.getString(0))
                }
            }
            return cached_init_date!!
        }
    }
}

class MainActivity : AppCompatActivity() {

    private val vm: VM by viewModels<VM>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        update()
        bind_events()
    }

    override fun onResume() {
        super.onResume()
        update()
        bind_events()
    }

    fun update() {
        calender.removeAllViews()
        calender.rowCount = 1 + 6
        for (day in arrayOf("Mon", "Tue", "Wen", "Thu", "Fri", "Sat", "Sun").iterator().withIndex()) {
            val day_v = TextView(this, null, 0, R.style.DayOfWeekText)
            day_v.setText(day.value)
            day_v.layoutParams = GridLayout.LayoutParams(GridLayout.spec(0), GridLayout.spec(day.index, 1, 1f)).apply {
                height = ViewGroup.LayoutParams.WRAP_CONTENT
            }
            calender.addView(day_v)
        }

        val begin = today.apply {
            set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
            add(Calendar.WEEK_OF_YEAR, -5)
        }
        val end = today.apply {
            set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
            if (this.before(today)) {
                this.add(Calendar.WEEK_OF_YEAR, 1)
            }
        }
        val current = begin.clone() as Calendar;
        val lst = vm.get_all_since(this, begin)
        var ptr = 0
        while(true) {
            if (current.after(end) && !current.equals(end)) {
                Log.d("c", date_fmt.format(current.time))
                Log.d("c", date_fmt.format(end.time))
                Log.d("c", date_fmt.format(today.time))
                break;
            }
            var curr_has_no = false;
            if (ptr < lst.size && lst.get(ptr).equals(current)) {
                curr_has_no = true;
                ptr ++;
            }
            calender.addView(get_day_view(today, current, ChronoUnit.DAYS.between(begin.toInstant(), current.toInstant()).toInt()/7 + 1, curr_has_no))
            current.add(Calendar.DAY_OF_WEEK, 1)
        }

        if (lst.isNotEmpty()) {
            val last = lst.last()
            no.isEnabled = !last.equals(today)
        } else {
            no.isEnabled = true
        }
    }

    fun get_day_view(today: Calendar, day: Calendar, week_row: Int, has_no: Boolean): View {
        val tv = TextView(this, null, 0, R.style.DayText)
        tv.layoutParams = GridLayout.LayoutParams(GridLayout.spec(week_row), GridLayout.spec(day.get(
            Calendar.DAY_OF_WEEK).run {
            if (this == 1) {
                6
            } else {
                this - 2
            }
        }, 1, 1f)).apply {
            height = ViewGroup.LayoutParams.WRAP_CONTENT
        }
        if (day.before(vm.get_init_date(this))) {
            tv.setTextColor(resources.getColor(R.color.past))
            tv.setText("?")
        } else if (day.after(today)) {
            tv.setText("?")
            tv.setTextColor(resources.getColor(R.color.future))
        } else if (has_no) {
            tv.setText("No.")
            tv.setTextColor(resources.getColor(R.color.no))
        } else {
            tv.setText("Yes.")
            if (today.equals(day)) {
                tv.setTextColor(resources.getColor(R.color.today))
            } else {
                tv.setTextColor(resources.getColor(R.color.yes))
            }
        }
        return tv;
    }

    val date_change_receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            update()
        }
    }

    fun bind_events() {
        no.setOnClickListener {
            put_today()
        }
        try {
            this.unregisterReceiver(date_change_receiver)
        } catch (e: IllegalArgumentException) {}
        this.registerReceiver(date_change_receiver, IntentFilter(Intent.ACTION_DATE_CHANGED))
    }

    override fun onPause() {
        super.onPause()
        try {
            this.unregisterReceiver(date_change_receiver)
        } catch (e: IllegalArgumentException) {}
    }

    fun put_today() {
        vm.date_put_no(this, today)
        update()
    }
}
