package org.maowtm.android.no

import java.text.SimpleDateFormat
import java.util.*

abstract class Utils {
    companion object {
        val date_fmt = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        val today
            get() = Calendar.getInstance(Locale.US).apply {
                clear(Calendar.MILLISECOND)
                clear(Calendar.SECOND)
                clear(Calendar.MINUTE)
                set(Calendar.HOUR_OF_DAY, 0)
            }

        fun parse_date(c: String): Calendar
            = Calendar.Builder().setLocale(Locale.US).setInstant(date_fmt.parse(c)!!).build().apply {
                clear(Calendar.MILLISECOND)
                clear(Calendar.SECOND)
                clear(Calendar.MINUTE)
                set(Calendar.HOUR_OF_DAY, 0)
            }
    }
}