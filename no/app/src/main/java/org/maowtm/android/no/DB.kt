package org.maowtm.android.no

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.lang.UnsupportedOperationException
import org.maowtm.android.no.Utils.Companion.date_fmt
import org.maowtm.android.no.Utils.Companion.today

class DB(ctx: Context) : SQLiteOpenHelper(ctx, "db", null, 1) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("""CREATE TABLE "no" (
                                "date"	TEXT NOT NULL UNIQUE,
                                PRIMARY KEY("date")
                            );""")
        db.execSQL("""CREATE TABLE "init" ("date" TEXT NOT NULL);""")
        db.execSQL("""INSERT INTO "init" VALUES (?);""", arrayOf(date_fmt.format(today.time)))
    }

    override fun onUpgrade(db: SQLiteDatabase, old_version: Int, new_version: Int) {
        if (old_version == new_version) return;
        if (old_version == 1) {
            db.execSQL("""DROP TABLE "no";""")
            db.execSQL("""DROP TABLE "init";""")
            this.onCreate(db)
            return;
        }
        throw UnsupportedOperationException()
    }

    companion object {
        var current_instance: DB? = null

        fun ensure_open(ctx: Context) {
            if (current_instance == null) {
                current_instance = DB(ctx)
            }
        }

        fun db(ctx: Context): SQLiteDatabase {
            this.ensure_open(ctx)
            return current_instance!!.writableDatabase
        }
    }
}