package org.maowtm.android.sleepearly

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.widget.GridLayout
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.sleep_record.view.*
import java.time.Duration
import java.util.*

class MainActivity : AppCompatActivity() {

    var update_timer: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        update_timer = Handler(this.mainLooper)
        update()
        bind_events()
        startService(Intent(this, AwakeDetectorService::class.java))

        update_timer!!.removeCallbacks(update_run)
        update_timer!!.postDelayed(update_run, 500)
    }

    fun update() {
        val s = Storage.get_instance(this)
        val dt = DateTimeUtils(s)
        val now = dt.now
        val chd = dt.normalize_time_to_hday(now)

        goal_text_sleep.setText(dt.format_duration_as_time(s.get_target_sleep_before()))
        goal_text_get_up.setText(dt.format_duration_as_time(s.get_target_wake_before()))

        if (!now.isBefore(dt.earlist_sleep_time_of(dt.current_hday))) {
            group_sleep_countdown.visibility = View.VISIBLE
            val time_left = Duration.between(now, dt.target_sleep_time_of(chd))
            val time_left_abs = time_left.abs()
            countdown.setText(String.format("%s%02d:%02d:%02d", if (time_left.isNegative) {"-"} else {""}, time_left_abs.toHours(), time_left_abs.toMinutes()%60, time_left_abs.seconds%60))
            countdown.setTextColor(if (time_left.isNegative) {
                resources.getColor(R.color.red)
            } else {
                resources.getColor(R.color.normaltext)
            })
        } else {
            group_sleep_countdown.visibility = View.GONE
        }

        log.removeAllViews()
        for (record in s.query_sleeps_backward(chd, 14)) {
            val ll = LinearLayout(this)
            log.addView(ll)
            layoutInflater.inflate(R.layout.sleep_record, ll, true)
            ll.date.setText(String.format("%04d-%02d-%02d (%d days ago)", record.hday.get(Calendar.YEAR), record.hday.get(Calendar.MONTH), record.hday.get(Calendar.DAY_OF_MONTH),
                Duration.between(record.hday.time.toInstant(), chd.time.toInstant()).toDays()))
            ll.sleep_time.setText(String.format("Sleep: %s%s", dt.format_time(record.sleep_time), if (record.hday.equals(chd)) {"\u2026?"} else {""}))
            if (record.sleep_time.isAfter(dt.hmidnight_of(record.hday).plus(s.get_target_sleep_before()))) {
                ll.sleep_time.setTextColor(resources.getColor(R.color.red))
            }
            if (record.wake_time == null) {
                ll.wake_time.setText("Wake: ??")
            } else {
                ll.wake_time.setText(String.format("Wake: %s", dt.format_time(record.wake_time)))
                if (record.wake_time.isAfter(dt.hmidnight_of(record.hday).plus(s.get_target_wake_before()))) {
                    ll.wake_time.setTextColor(resources.getColor(R.color.red))
                }
            }
        }

        bind_events()
    }

    val update_run = object : Runnable {
        override fun run() {
            this@MainActivity.update()
            update_timer!!.removeCallbacks(this)
            update_timer!!.postDelayed(this, 200)
        }
    }

    fun bind_events() {
    }

    override fun onPause() {
        super.onPause()
        this.update_timer!!.removeCallbacks(update_run)
    }

    override fun onResume() {
        super.onResume()
        update_timer!!.removeCallbacks(update_run)
        update_timer!!.postDelayed(update_run, 200)
        update()
    }
}
