package org.maowtm.android.sleepearly

import java.time.Duration
import java.time.Instant
import java.util.*

class DateTimeUtils(val s: Storage) {
    val now: Instant
        get() = Instant.now()
    val current_hday: Calendar
        get() = normalize_time_to_hday(now)

    fun normalize_time_to_hday(t: Instant): Calendar
        = Calendar.Builder().setCalendarType("gregorian").setInstant(t.minus(s.get_hday_start_from()).toEpochMilli()).build().apply {
            set(Calendar.MILLISECOND, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.HOUR_OF_DAY, 0)
        }
    fun hmidnight_of(hday: Calendar): Instant
        = (hday.clone() as Calendar).apply {
            add(Calendar.DAY_OF_YEAR, 1)
        }.time.toInstant()

    fun target_sleep_time_of(hday: Calendar): Instant
        = hmidnight_of(hday).plus(s.get_target_sleep_before())
    fun target_wake_time_of(hday: Calendar): Instant
        = hmidnight_of(hday).plus(s.get_target_wake_before())
    fun earlist_sleep_time_of(hday: Calendar): Instant
        = hmidnight_of(hday).plus(s.get_earlist_sleep_time())

    fun format_duration_as_time(d: Duration): String {
        val d2 = if (d.isNegative) { d.plus(Duration.ofDays(1)) } else { d }
        return String.format("%02d:%02d", d2.toHours(), d2.toMinutes()%60)
    }
    fun format_time(t: Instant): String {
        val c = Calendar.Builder().setInstant(t.toEpochMilli()).build()
        return String.format("%02d:%02d", c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE))
    }
}