package org.maowtm.android.sleepearly

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import android.os.IBinder
import android.os.PowerManager

class AwakeDetectorService : Service() {

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    var s: Storage? = null
    var handler: Handler? = null

    var unlocked = false
    var registered_receiver = false
    val unlock_receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            unlocked = true
            save_awake_repeater.run()
        }
    }
    val screen_off_receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (unlocked) {
                awake_update()
            }
            unlocked = false
            handler!!.removeCallbacks(save_awake_repeater)
        }
    }
    val save_awake_repeater = object : Runnable {
        override fun run() {
            if (unlocked) {
                awake_update()
                handler!!.removeCallbacks(this)
                handler!!.postDelayed(this, 1000 * 60 * 20)
            }
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        s = Storage.get_instance(this)
        if (handler == null) {
            handler = Handler(this.mainLooper)
        }
        if (!registered_receiver) {
            registered_receiver = true
            registerReceiver(unlock_receiver, IntentFilter(Intent.ACTION_USER_PRESENT))
            registerReceiver(screen_off_receiver, IntentFilter(Intent.ACTION_SCREEN_OFF))
            unlocked = (getSystemService(Context.POWER_SERVICE) as PowerManager).isInteractive
            handler!!.removeCallbacks(save_awake_repeater)
            if (unlocked) {
                save_awake_repeater.run()
            }
        }
        return START_STICKY
    }

    override fun onDestroy() {
        unregisterReceiver(unlock_receiver)
        unregisterReceiver(screen_off_receiver)
        handler?.removeCallbacks(save_awake_repeater)
        registered_receiver = false
        super.onDestroy()
    }

    fun awake_update() {
        s!!.user_awake()
    }
}
