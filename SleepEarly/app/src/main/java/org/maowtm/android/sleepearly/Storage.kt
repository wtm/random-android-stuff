package org.maowtm.android.sleepearly

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import androidx.core.database.getLongOrNull
import java.lang.UnsupportedOperationException
import java.time.Duration
import java.time.Instant
import java.util.*
import kotlin.collections.ArrayList

class Storage private constructor(ctx: Context): SQLiteOpenHelper(ctx, "db", null, 1) {
    val dt = DateTimeUtils(this)

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("""CREATE TABLE "config" (name TEXT NOT NULL UNIQUE, value TEXT, PRIMARY KEY("name"));""")
        config_put_long(db, CONFIG_FIRST_RUN, serialize_time(dt.now))
        config_put_long(db, CONFIG_LAST_AWAKE_TOUCH, serialize_time(dt.now))
        config_put_long(db, CONFIG_HDAY_START_MILLIS, Duration.ofHours(5).toMillis())
        config_put_long(db, CONFIG_TARGET_SLEEP_BEFORE_MILLIS, Duration.ofMinutes(-30).toMillis())
        config_put_long(db, CONFIG_TARGET_WAKE_BEFORE_MILLIS, Duration.ofHours(9).toMillis())
        config_put_long(db, CONFIG_EARLIST_SLEEP_TIME_MILLIS, Duration.ofHours(-(24-20)).toMillis())
        db.execSQL("""CREATE TABLE "sleep" (
            "hday" INTEGER NOT NULL UNIQUE,
            "sleep_time" INTEGER NOT NULL,
            "wake_time" INTEGER,
            PRIMARY KEY("hday")
        );""")
        Log.d("db", "init")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (oldVersion == newVersion) return;
        if (oldVersion > 1 || newVersion < 1 || oldVersion < 1 || newVersion > 1) throw UnsupportedOperationException()
    }

    fun user_awake() {
        val db = writableDatabase
        val now = dt.now
        val chd = dt.normalize_time_to_hday(now)
        // update today's sleep
        if (!now.isBefore(dt.earlist_sleep_time_of(chd))) {
            if (db.rawQuery("""SELECT null FROM "sleep" WHERE "hday" = CAST (? AS INTEGER);""", arrayOf(serialize_date(chd).toString())).use { it.count } == 0) {
                db.execSQL("""INSERT INTO "sleep" ("hday", "sleep_time", "wake_time") VALUES (?, ?, NULL);""", arrayOf(
                    serialize_date(chd), serialize_time(now)))
            } else {
                db.execSQL("""UPDATE "sleep" SET "sleep_time" = ? WHERE "hday" = ?;""", arrayOf(
                    serialize_time(now), serialize_date(chd)))
            }
        } else {
            db.execSQL("""DELETE FROM "sleep" WHERE "hday" = ?;""", arrayOf(serialize_date(chd)))
        }
        // update yesterday's wake
        val yesterday = (chd.clone() as Calendar).apply {
            add(Calendar.DAY_OF_YEAR, -1)
        }
        db.execSQL("""UPDATE "sleep" SET "wake_time" = ? WHERE "hday" = ? AND ("wake_time" IS NULL OR "wake_time" > ?);""", arrayOf(
            serialize_time(now), serialize_date(yesterday), serialize_time(now)
        ))
        config_put_long(db, CONFIG_LAST_AWAKE_TOUCH, serialize_time(now))

        db.rawQuery("""SELECT * FROM "sleep";""", arrayOf()).use { res ->
            Log.d("dump", res.columnNames.joinToString(", "))
            res.moveToFirst()
            while (!res.isAfterLast) {
                Log.d("dump", (0 until res.columnCount).map { res.getString(it) }.joinToString(", "))
                res.moveToNext()
            }
        }
    }

    fun get_last_awake_touch_time(): Instant
        = deserialize_time(config_get_long(readableDatabase, CONFIG_LAST_AWAKE_TOUCH))

    fun get_hday_start_from(): Duration
        = Duration.ofMillis(config_get_long(readableDatabase, CONFIG_HDAY_START_MILLIS))

    fun get_target_sleep_before(): Duration
        = Duration.ofMillis(config_get_long(readableDatabase, CONFIG_TARGET_SLEEP_BEFORE_MILLIS))

    fun get_target_wake_before(): Duration
        = Duration.ofMillis(config_get_long(readableDatabase, CONFIG_TARGET_WAKE_BEFORE_MILLIS))

    fun get_earlist_sleep_time(): Duration
        = Duration.ofMillis(config_get_long(readableDatabase, CONFIG_EARLIST_SLEEP_TIME_MILLIS))

    data class SleepRecord(val hday: Calendar, val sleep_time: Instant, val wake_time: Instant?)

    fun query_sleeps_backward(starting_from: Calendar, limit: Int): List<SleepRecord> {
        return readableDatabase.rawQuery("""SELECT "hday", "sleep_time", "wake_time" FROM "sleep" WHERE "hday" <= CAST(? AS INTEGER) ORDER BY "hday" DESC LIMIT $limit;""", arrayOf(
            serialize_date(starting_from).toString())).use { res ->
            val lst = ArrayList<SleepRecord>(res.count)
            res.moveToFirst()
            while (!res.isAfterLast) {
                val wake_time = res.getLongOrNull(2)
                lst.add(SleepRecord(deserialize_date(res.getLong(0)), deserialize_time(res.getLong(1)), if (wake_time != null) { deserialize_time(wake_time) } else { wake_time }))
                res.moveToNext()
            }
            lst
        }
    }

    companion object {
        fun serialize_date(c: Calendar): Long
                = c.timeInMillis
        fun serialize_time(c: Instant): Long
                = c.toEpochMilli()
        fun deserialize_date(d: Long): Calendar
                = Calendar.Builder().setCalendarType("gregorian").setInstant(d).build().apply {
            set(Calendar.MILLISECOND, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.HOUR_OF_DAY, 0)
        }
        fun deserialize_time(d: Long): Instant
                = Instant.ofEpochMilli(d)
        fun config_get_long(db: SQLiteDatabase, name: String): Long {
            return db.rawQuery("""SELECT "value" FROM "config" WHERE "name" = ?;""", arrayOf(name)).use {
                it.moveToFirst()
                it.getLong(0)
            }
        }
        fun config_put_long(db: SQLiteDatabase, name: String, value: Long) {
            if (db.rawQuery("""SELECT null FROM "config" WHERE "name" = ?;""", arrayOf(name)).use { it.count > 0 }) {
                db.execSQL("""UPDATE "config" SET "value" = ? WHERE "name" = ?;""", arrayOf(value, name))
            } else {
                db.execSQL("""INSERT INTO "config" ("name", "value") VALUES (?, ?);""", arrayOf(name, value))
            }
        }

        val CONFIG_FIRST_RUN = "first_run"
        val CONFIG_LAST_AWAKE_TOUCH = "last_user_awake_touch"

        /**
         * Duration since 00:00
         */
        val CONFIG_HDAY_START_MILLIS = "hday_start"

        /**
         * Duration since 24:00
         */
        val CONFIG_TARGET_SLEEP_BEFORE_MILLIS = "target_sleep_before"

        /**
         * Duration since 24:00 of the day of sleep
         */
        val CONFIG_TARGET_WAKE_BEFORE_MILLIS = "target_wake_before"

        /**
         * Duration since 24:00
         */
        val CONFIG_EARLIST_SLEEP_TIME_MILLIS = "earlist_sleep"

        private var instance: Storage? = null
        @Synchronized fun get_instance(ctx: Context): Storage {
            if (instance == null) {
                val s = Storage(ctx)
                instance = s
                return s
            } else {
                return instance!!
            }
        }
    }
}